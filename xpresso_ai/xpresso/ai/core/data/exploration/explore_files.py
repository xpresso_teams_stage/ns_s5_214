__all__ = ["ExploreFiles"]
__author__ = ["Srijan Sharma"]

from xpresso.ai.core.data.exploration.explore_numeric import  ExploreNumeric

class ExploreFiles():

    def __init__(self,data):
        self.data = data
        return

    def populate_filesize(self):
        return ExploreNumeric(self.data).populate_numeric()